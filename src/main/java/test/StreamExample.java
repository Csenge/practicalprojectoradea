package test;

import edu.sda.practicalProject.model.Client;
import edu.sda.practicalProject.service.ClientService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {
        ClientService clientService = new ClientService();
        List<String> words = Arrays.asList("unu", "doi", "trei", "patru", "cinci");

        Stream<String> wordStream = words.stream();

        //list iteration
        for (String word: words) {
            System.out.println(word);
        }

        //stream iteration
        wordStream.forEach(word -> System.out.println(word));

        //map - maparea a fiecarei obiect dintr-o lista/ stream la un alt obiect
        // mapping on a list
        List<String> mappedList = new ArrayList<>();

        for (String word: words) {
            mappedList.add(word + "testing list");
        }
            // for testing purposes we print the list
        mappedList.forEach(currentWord -> System.out.println(currentWord));

        //mapping on a stream - and printing it - no return value
//        mappedList.stream()
//                .map(currentWord -> currentWord = currentWord + " test stream")
//                .forEach(currentNewWord -> System.out.println(currentNewWord));


        // save the new values in a new stream
        Stream<String> newWords = mappedList.stream()
                .map(currentWord -> currentWord = currentWord + " test stream");

        // save the new values in a new list
        List<String> newWordsList = mappedList.stream()
                .map(currentWord -> currentWord = currentWord + " test stream")
               .collect(Collectors.toList());

        // real life example:
        // primesc ca si input o lista de id-uri care sa reprezinte lista produselor cumparate de cineva
        // trebuie sa fac o lista cu produsele efective bazat pe lista de id-uri primite


        //input : lista de id-uri
        List<Integer> idList = Arrays.asList(1234, 3456, 9876);
        // outputul dorit
        List<Client> clientList;

        // cum populam outputul based on input
        clientList = idList.stream() // se face stream din lista de id-uri
                .map(currentId -> clientService.getClientByCNP(String.valueOf(currentId))) // fiecare id se transforma intr-un obiect de tip Client
                .collect(Collectors.toList()); // stream-ul se transforma in lista

        clientList.stream().forEach(client -> System.out.println(client));


    }
}
