package test;

import java.util.*;

public class DictionariesExample {

    public static void main(String[] args) {
        // List< ce vine aici? > - intre < > o sa vina definitia exacta a map-ului
        // ca sa il putem parcurge fara sa folosim Object
        // daca il lasam List<Map> al doilea for trebuia sa foloseasca Object
        List<Map<String, List<String>>> dictionareDeSinonime = new ArrayList<>();
        Map<String, List<String>> dictionarDeSinonimeRomana = new HashMap();
        Map<String, List<String>> dictionarDeSinonimeEngleza = new HashMap();

        dictionareDeSinonime.add(dictionarDeSinonimeRomana);
        dictionareDeSinonime.add(dictionarDeSinonimeEngleza);

        List<String> sinonimeCasa = new ArrayList<>();
        sinonimeCasa.add("imobil");
        sinonimeCasa.add("apartament");
        dictionarDeSinonimeRomana.put("casa", sinonimeCasa);

        dictionarDeSinonimeEngleza.put("house", Arrays.asList("building", "flat"));

        //todo try and add other words and synonyms

        // iteram peste lista care contine map-urile
        for (Map<String, List<String>> currentDictionary: dictionareDeSinonime) {
            // pentru fiecare map iteram peste key set sa
            // obtinem cuvantul si sa obtinem si sinonimele cuvantului

            for (String keyDinCurrentDictionarDeSinonime: currentDictionary.keySet()) {
                System.out.println("Cuvantul: " + keyDinCurrentDictionarDeSinonime);
                System.out.println("Sinonimele: "
                        + currentDictionary.get(keyDinCurrentDictionarDeSinonime));
            }
        }

        String stringExample = "asdsa";
        String sringExampleReversed = new StringBuilder(stringExample).reverse().toString();

        boolean isStringPal = stringExample.equalsIgnoreCase(sringExampleReversed);

        System.out.println(isStringPal);

        int intExample = 13431;
        String intExampleString = String.valueOf(intExample);
//        String intExampleStringReversed

        String intExampleS2 = intExampleString + "";
    }
}
