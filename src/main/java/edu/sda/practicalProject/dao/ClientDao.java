package edu.sda.practicalProject.dao;

import edu.sda.practicalProject.config.HibernateConfig;
import edu.sda.practicalProject.model.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientDao {

    List<Client> clients = new ArrayList<>();

    public void insertClient(Client client) {
        //inserting client to db
        Transaction transaction = null;

        try {
            Session session = HibernateConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(client);
            transaction.commit();

        } catch (Exception e) {
            transaction.rollback();
        }

        //insert to list
        clients.add(client);
    }

    public List<Client> getAllClients() {
        Session session = HibernateConfig.getSessionFactory().openSession();
        List<Client> clients = session.createQuery("from Client", Client.class).list();
        return clients;
    }

    public Client getClientByName(String name) {
        return null; //todo
    }

    public Client getClientByCNP(String cnp) {
        return null; //todo
    }

    public void removeClient(Client client) {
        //todo
    }
}
