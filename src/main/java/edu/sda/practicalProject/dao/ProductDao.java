package edu.sda.practicalProject.dao;

import edu.sda.practicalProject.config.HibernateConfig;
import edu.sda.practicalProject.model.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class ProductDao {
    
    List<Product> productList = new ArrayList<>();

    public void insertProductToDB(Product product) {
        Transaction transaction = null;
        try {
            Session session = HibernateConfig.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(product);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }

        System.out.println("Inserting product " + product);
        productList.add(product);
    }

    public List<Product> getAllProducts() {

        Session session = HibernateConfig.getSessionFactory().openSession();
        return session.createQuery("from Product", Product.class).list();
// no need to specify select *, it is enough to say from Product
        // HQL - Hibernate Query Language
        // vs ce folosesti in mysql e MySQL - sequential query language
        // HQL o sa poate sa fie tradus in sql

        //return productList;
    }

    public Product getProductByName(String productName) {
        // how to find product by name?
        // iterate over the list and find product with the name recived as parameter

        // instead of getAllProducts we had productList - a simple list in ths class containing all products
        // NOW: we use the method getAllProducts which returns the list of all products in DB
        for (Product currentProduct: getAllProducts()) {
            if (currentProduct.getProductName().equalsIgnoreCase(productName)){
                return currentProduct;
            }
        }
        return null;
    }

    public void removeProduct(Product productToDelete) {
        //remove product from db
        Transaction transaction = null;
        try {
            Session session = HibernateConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(productToDelete);
            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        //remove product from list
        productList.remove(productToDelete);
    }

    public void updateProduct(Product productToUpdate) {
        Transaction transaction = null;
        try {
            Session session = HibernateConfig.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            // update an entity - if entity to update cannot be found - will not do anything
            session.update(productToUpdate);
            // if entity can be found -> update, otherwise entity will be created
            //session.saveOrUpdate(productToUpdate);

            transaction.commit();

        } catch (Exception e) {
            transaction.rollback();
        }
    }

    public void updateProductName(Product productToUpdate) {
        Transaction transaction = null;
        try {
            Session session = HibernateConfig.getSessionFactory().openSession();

            transaction = session.beginTransaction();
            /**
             * Not sure of what I am doing so I will take an example from stackoverflow
             *
             *
             * Query q = session.createQuery("from ModelClassname where
             * ClassVariableId= :ClassVariableId");
             * q.setParameter("ClassVariableId", 001);
             *
             * ModelClassname result = (ModelClassname)q.list().get(0);
             */
            Query updateQuery = session.createQuery("update Product set productName= :newProductName" +
                    " where id= :idOfEntityToUpdate");
            updateQuery.setParameter("newProductName", productToUpdate.getProductName());
            updateQuery.setParameter("idOfEntityToUpdate", productToUpdate.getId()); // find parameter by name

            int result = updateQuery.executeUpdate();

            transaction.commit();
            System.out.println("My custom update resulted in : " + result);



        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
