package edu.sda.practicalProject.service;

import edu.sda.practicalProject.dao.ClientDao;
import edu.sda.practicalProject.model.Client;

import java.util.List;
import java.util.stream.Collectors;

public class ClientService {

    private ClientDao clientDao = new ClientDao();

    public void insertClient(String name, String cnp, int age) throws Exception {
        Client client = new Client();
        client.setName(name);

        if (isValidCNP(cnp)) {
            //set cnp
            client.setCnp(cnp);
            System.out.println("CNP is valid: " + cnp);
        } else {
            //do not se cnp or throw exception
            client.setCnp(null);
            System.out.println("CNP is NOT valid: " + cnp);

            //varianta 1 - handle the exception immediately
//            try {
//                // arunc o exceptie pt ca cnp-ul nu este valid
//                throw new Exception("INVALID CNP");
//            } catch (Exception e) {
//                //prind exceptia imediat dupa ce l-am aruncat -> il printez si totul se continua de parca nu s-ar fi intamplat nimic
//                e.printStackTrace();
//            }

            //varianta 2 - do not handle the exception here - do not continue to execute this method anymore
            // in this case insert will not be called
            throw new Exception("INVALID CNP");
        }

        client.setAge(age);
        clientDao.insertClient(client);
    }

    private boolean isValidCNP(String cnp) {
        if (cnp.length() != 10) {
            return false;
        }
        return true;
    }

    public List<Client> getAllClients() {
        //using client dao get all clients from db
        List<Client> clientList = clientDao.getAllClients();
        return clientList;
    }

    public Client getClientByName(String name) {
        return clientDao.getClientByName(name);
    }

    public Client getClientByCNP(String cnp) {
        List<Client>  clientsMatchingCNP = clientDao.getAllClients()
                .stream()
                .filter(client -> client.getCnp().equalsIgnoreCase(cnp))
                .collect(Collectors.toList());

        if (clientsMatchingCNP.size() > 1) {
            throw new RuntimeException("CANNOT HAVE TWO CLIENTS WITH THE SAME CNP");
        }
        if (clientsMatchingCNP.isEmpty()) {
            throw new RuntimeException("No client with cnp: " + cnp);
        }

        return clientsMatchingCNP.get(0);  // get first element from index 0
    }

    public void removeClientByName(String name) {
        //step 1 - identify client by name

        //step 2 - remove client

    }
}
