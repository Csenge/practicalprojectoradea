package edu.sda.practicalProject.service;

import edu.sda.practicalProject.dao.ProductDao;
import edu.sda.practicalProject.model.Product;

import java.util.List;

public class ProductService {

    // pentru a instantia variabila asta globala avem 3 optiuni
    // 1. aici il instantiem direct cu = new ProductDao();
    // 2. sa facem getter/ setter si sa le folosim
    // 3. prin constructor
    private ProductDao productDao;

    public ProductService() {
        this.productDao = new ProductDao();
    }

    public void insertNewProduct(String productName, float price) {
        Product product = new Product();
        product.setProductName(productName);
        product.setPrice(price);

        // possible validation:
        // before inserting check if a product with this name exists
        // if exists - do not insert - throw exception or stg
        // else insert product

        productDao.insertProductToDB(product);

    }

    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }

    public void removeProduct(String name) {
        // pasul 1 - se cauta produsul cu numele primit ca si parametru in db/lista
        Product productFoundByName = productDao.getProductByName(name);

        System.out.println("Found item: " + productFoundByName);
        //pasul 2 - dupa identificarea produsului - se sterge produsul
        productDao.removeProduct(productFoundByName);
    }

    public void updateProduct() {
        // version 1
        Product newCola = new Product();
        newCola.setId(1);
        //newCola.setPrice(3f);
        newCola.setProductName("New Price Cola");
        // folosind versiunea asta cu setPrice comentat pretul nou o sa fie 0
        // de ce? pentru ca query-ul default de la hibernate face update
        //la toate fieldurile din obiect - in cazul nostru Product
        // identifica obiectul prin id - asa ca id nu se va schimba
        // face update la fieldurile pe care le-am modificat noi manual
        // in cazul asta productName
        // face update la fielduri nemodificate si le pune valoarea default
        // in cazul pretului care e float default e 0
        // daca punem pretul ca si Float pretul nou o sa fie null


        // todo see how we used to update products
        //productDao.updateProduct(newCola);

        // versiune 2 - necesita modificari in ProductDao
        // in momentul de fata HQL-ul care ruleaza este:
        //update Product set price=?, productName=? where id=?
        // putem modifica query-ul asta ca sa ruleze update doar pe productName

        productDao.updateProductName(newCola);
    }

}
