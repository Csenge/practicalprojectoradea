package edu.sda.practicalProject;

import edu.sda.practicalProject.model.Client;
import edu.sda.practicalProject.model.Product;
import edu.sda.practicalProject.service.ClientService;
import edu.sda.practicalProject.service.ProductService;

import java.util.*;

public class ApplicationRunner {
    /**
     * This class will be the entry point to my online shop. In the main class I will have initialization data
     * and I will call the operations that will be defined in the future. TODO define operations.
     *
     */

    public static void main(String[] args) {
        ProductService productService = new ProductService();
        ClientService clientService = new ClientService();

        //step 1: create a new product and add it to db
       productService.insertNewProduct("Cola", 3.5f);
       productService.insertNewProduct("Biscuiti", 5.5f);
       productService.insertNewProduct("Fanta", 5.5f);
       productService.insertNewProduct("Apa", 5.5f);
       productService.insertNewProduct("Morcovi", 5.5f);
       productService.insertNewProduct("Mere", 5.5f);
       productService.insertNewProduct("Cartofi", 5.5f);
       productService.insertNewProduct("Carne", 5.5f);

       //step 2: read all products
        List<Product> products = productService.getAllProducts();

        System.out.println("The products in our shop are: ");
        for (Product currentProduct : products) {
            System.out.println(currentProduct);
        }

        //step 2.1: read product by name


        //step 3: update product by name - find product by name, add a new price

        // step 4: delete product by name - find by name and delete it
        productService.removeProduct("Biscuiti");

        //step 2: read all products
        List<Product> products2 = productService.getAllProducts();

        System.out.println("The products in our shop are: ");
        for (Product currentProduct : products2) {
            System.out.println(currentProduct);
        }

        productService.updateProduct();


        //varianta 1
//        try {
//            clientService.insertClient("Ana", "2647438543", 20);
//            clientService.insertClient("Ion", "1456789098763", 22);
//            clientService.insertClient("Elena", "2456789-09876", 30);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //varianta 2
//        clientService.insertClient("Ana", "2647438543", 20);
//        clientService.insertClient("Ion", "1456789098763", 22);
//        clientService.insertClient("Elena", "2456789-09876", 30);

        //varianta 3
        try {
            clientService.insertClient("Ana", "2647438543", 20);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            clientService.insertClient("Ion", "1456789098763", 22);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            clientService.insertClient("Elena", "2456789-09876", 30);
        } catch (Exception e) {
            e.printStackTrace();
        }


        /**
         * Exceptions
         *
         * 1. can be immediately handled using try -catch
         * 2. can be thrown so that the exception must be handled from the caller method (ex. cnp validation see above.
         *  In care from service we throw the exception - we must handle it here, in Application Runner. See handling options above varianta 1 or 2 or 3)
         *
         *  If there can be multiple exceptions thrown in the same flow for example in a checkout flow exceptions can be thrown because
         *  cart is empty, the client does not provide all the necessary data to checkout, card payment fails, confirmation mails
         * sending fails, etc. consider using a try catch and catch the exeptions one by one. See example below. Start with custom
         * exceptions and finish with Exception. Otherwise the catch (Exception e)  will catch all the exceptions and the custom ones will
         * never apply.
         *
         *
         */
        try {


            // send order - if order is empty exception is thrown

            //send confirmation mail - if cannot be sent exception is thrown



        } // catch (CartEmptyException cee) {}
        // catch (EmailException ee) {}


        catch (NumberFormatException e) {
            // exceptia de aici poate sa ajunga ori din sendOrder ori din sendConfirmationMail

        } catch (Exception nfe) {

        }

        // find client by name and print the identified client
        // find client by cnp and print the found client

        //remove a client
        //print the list of remaining clients (all of them)
        List<Client> clients = clientService.getAllClients();

        for (Client currentClient: clients) {
            System.out.println(currentClient);
        }


        //MAP examples
        // map exemplu 1 - nu are legatura cu ce am facut pana acum
        // se creeaza un map simplu cu nume - as key, numele unui produs- as value
        // pas 1 se insereaza cateva entry-uri
        //pas 2 - se printeaza map
        Map<String, String> clientNameProductName = new HashMap<>();
        // this map will contain as key the name of a client and as value the client's
        // favourite product
        // every client will have a single favourite products

        clientNameProductName.put("Claudiu", "Carne");
        clientNameProductName.put("Claudia", "Biscuiti");
        clientNameProductName.put("Ana", "Mere");


        for (int i = 0; i< 10; i++) {}

        for (String mapKeyClientName: clientNameProductName.keySet()) {
            System.out.println("Client name is: " + mapKeyClientName);
            System.out.println("Client's favourite product and" +
                    " thus the value in the map is " +
                    clientNameProductName.get(mapKeyClientName));
        }

       // varianta 2 - vreau ca map-ul meu sa fie ordonat
       // keep in mind pentru varianta 1 printatul s-a facut in urmatoarea ordine: Claudia, Ana, Claudiu
       // acum dorim sa il printam in ordinea in care le-am adaugat
        Map<String, String> clientNameProductNameOrdered = new LinkedHashMap<>();
        // aceeasi chestie se plica la liste folosind LinkedList

        clientNameProductNameOrdered.put("Claudiu", "Carne");
        clientNameProductNameOrdered.put("Claudia", "Biscuiti");
        clientNameProductNameOrdered.put("Ana", "Mere");

        for (String mapKeyClientName: clientNameProductNameOrdered.keySet()) {
            System.out.println("Client name is: " + mapKeyClientName);
            System.out.println("Client's favourite product and" +
                    " thus the value in the map is " +
                    clientNameProductNameOrdered.get(mapKeyClientName));
        }

        // exercitiu map + online shop
        // map cu client care sa aiba o lista de produse
        // variante de incercat: (1) Map<Client, ArrayList<Product>> - lista de produse nu o sa
        //                                          fie sortat si poate sa aiba si duplicate
        //                      (2) Map<Client, LinkedList<Product>> - lista de produse o sa fie sortat
                                                    // ordinea in care le-am adaugat si poate sa aiba duplicate
        //                      (3) Map<Client, HashSet<Product>> - fara sortare,fara duplicate
        //                      (4) Map<Client, LinkedHashSet<Product>> - sortat fara duplicate

        //(1) - Map<Client, ArrayList<Product>>
        Map<Client, ArrayList<Product>> clientProductArrayMap = new HashMap<>();
        Client clientIon = new Client();
        clientIon.setName("Ion");
        Client clientDan = new Client();
        clientDan.setName("Dan");
        Client clientAna = new Client();
        clientAna.setName("Ana");
        //varianta 1 - my service class returns List, not ArrayList - so I must make
        // a downcast. I can do it without worries because List is more general and
        // arraylist implements list
        //ArrayList productsInDBArraylist = (ArrayList) productService.getAllProducts();

        //varianta 2
        ArrayList productsInDBArraylist = new ArrayList(); // create my empty list
        // add all the products from db to my arraylist
        // how to add multiple items in one step to a list
        productsInDBArraylist.addAll(productService.getAllProducts());

        // want to duplicate the first element of the list obtained from the db
        // in order to proove that in my arraylist i can have duplicates
        productsInDBArraylist.add(productsInDBArraylist.get(0));

        clientProductArrayMap.put(clientIon, productsInDBArraylist);
        clientProductArrayMap.put(clientDan, productsInDBArraylist);
        clientProductArrayMap.put(clientAna, productsInDBArraylist);


        // get value from map pt. client Dan
        ArrayList<Product> value = clientProductArrayMap.get(clientDan);
        // returneaza valoarea din map pentru cheia clientDan

        for (Client currentClient: clientProductArrayMap.keySet()
             ) {
            System.out.println("current client: " + currentClient);
            System.out.println("current client's list: " +
                    clientProductArrayMap.get(currentClient));
        }

        //(3) Map<Client, HashSet<Product>>
        // o sa folosesc clientii existenti si lista de produse numita productsInDBArraylist
        // care deja are un element duplicat

        Map<Client, HashSet<Product>> clientHashSetMap = new HashMap<>();

        HashSet<Product> productSet = new HashSet<>();
        productSet.addAll(productsInDBArraylist); // add all elements of the list even if it contains duplicates
        // to verify if the duplicates were removed

        clientHashSetMap.put(clientAna, productSet);
        clientHashSetMap.put(clientDan, productSet);
        clientHashSetMap.put(clientIon, productSet);

        for (Client currentClient: clientHashSetMap.keySet()
             ) {
            System.out.println("Current client: " + currentClient);
            System.out.println("Set of products for the current client: " +
                    clientHashSetMap.get(currentClient));
        }


        //todo rezolva aceeasi problema cu linkedlist si linkedhashset
        // pt rezolvare: se duplica liniile 230 - 246 se schimba tipul productHashSetului
        // se schimba si numele producthashset-ului ca sa nu avem duplicate
        // se adauga fiecare element din db in noul set/lista
        // se populeaza clientLinkedListMap / clientLinkedHashSetMap cu clients and products
        // se itereaza peste keyset si printam ce am printat si pana acum
    }
}
